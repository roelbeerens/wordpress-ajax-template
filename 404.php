<?php get_header(); ?>

<section id="main-content">
	<div id="content">
		<h1><?php _e("Sorry, Nothing was Found"); ?></h1>
		<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.' ); ?></p>
		<?php get_search_form(); ?>	
	<div id="content">
</section>

<?php get_footer(); ?>