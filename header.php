<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<!--[if IE]><![endif]-->
<title><?php wp_title( '|', true, 'right' ); ?> <?php bloginfo('name'); ?></title>
<meta name="viewport" content="width=device-width">
<?php if ( file_exists(TEMPLATEPATH .'/favicon.ico') ) : ?>
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
<?php endif; ?><?php if ( file_exists(TEMPLATEPATH .'/apple-touch-icon.png') ) : ?>
<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/apple-touch-icon.png">
<?php endif; ?><link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>">
<link href='//fonts.googleapis.com/css?family=Raleway:400,200,700' rel='stylesheet' type='text/css'>
<?php wp_head(); ?>
</head><?php $body_classes = join( ' ', get_body_class() ); ?>
<!--[if lt IE 7 ]><body class="ie6 <?php echo $body_classes; ?>"><![endif]-->
<!--[if IE 7 ]><body class="ie7 <?php echo $body_classes; ?>"><![endif]-->
<!--[if IE 8 ]><body class="ie8 <?php echo $body_classes; ?>"><![endif]-->
<!--[if IE 9 ]><body class="ie9 <?php echo $body_classes; ?>"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><body class="<?php echo $body_classes; ?>"><!--<![endif]-->

<div class="grid">			
	<header id="header" role="banner" class="hide-for-small">
		<?php if (is_home() || is_front_page()) : ?>
			<h1 id="logo"><a href="<?php bloginfo('url'); ?>/"><?php bloginfo('name'); ?></a></h1>
		<?php else : ?>
			<div id="logo"><a href="<?php bloginfo('url'); ?>/"><?php bloginfo('name'); ?></a></div>			
		<?php endif; ?>
	</header>
</div>

<div class="grid">
	<nav id="main-nav" class="main-nav" role="navigation"><?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?></nav>
	<div class="mobile-menu-holder show-for-small">
		<header class="main-header">
			<a href="#" class="open-menu">
				<img src="<?php bloginfo('template_url');?>/images/ui/mobile-menu.gif" />
			</a>
			<a href="#" class="close-menu">
				<img src="<?php bloginfo('template_url');?>/images/ui/mobile-menu.gif" />
			</a>
			<h1><?php bloginfo('name'); ?></h1>
		</header>
	</div>
</div>

<div class="grid">
	<div id="main" role="main">